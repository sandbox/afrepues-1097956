
What is this?
-------------

Drush ACLs is an extension created with the goal of providing an easy
way to a choosen set of filesystem permissions to a Drupal deployment,
and optionally help you keep them by hooking into Drush commands to
apply the permissions as new code is deployed.


What is implemented?
--------------------

At the moment only the hooks are being developed, after that a drush
command will be implemented that will allow applying a set of
permissions.


Maintainer
----------
- Servilio Afre Puentes (afrepues@mcmaster.ca)

